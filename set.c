#include <stdlib.h>
#include "set.h"

#define TS 1000

size_t hash(CFG x)
{
    x /= 23;
    return x % TS;
}

typedef struct setel *Setel;
struct setel {
    State st;
    struct setel *next;
};

struct set {
    struct setel *table[TS];
    bool sorted;
};

typedef struct binode *Binode;
struct binode {
    State st;
    struct binode *left, *right;
} *BROOT;

Set set_new(bool sort)
{
    int i;
    Set s = malloc(sizeof(struct set));

    for (i = 0; i < TS; ++i) s->table[i] = NULL;
    if (s->sorted = sort)
        BROOT = NULL;
    return s;
}

static void sorted_insert(State st)
{
    Binode bn, nn = malloc(sizeof(struct binode));

    nn->st = st; nn->left = nn->right = NULL;
    if (!(bn = BROOT))
        BROOT = nn;
    else {
        while (bn->left || bn->right) {
            while (bn->left && state_cmp(st, bn->left->st) < 0)
                bn = bn->left;
            while (bn->right && state_cmp(st, bn->right->st) >= 0)
                bn = bn->right;
            if (!bn->left && state_cmp(st, bn->st) < 0) {
                bn->left = nn; break;
            } else if (!bn->right && state_cmp(st, bn->st) >= 0) {
                bn->right = nn; break;
            }
        }
    }
}

void set_insert(Set s, State *_st)
{
    State st = *_st; *_st = NULL;
    size_t pos = hash(state_cfg(st));
    Setel sel = malloc(sizeof(struct setel));

    sel->st = st;
    sel->next = s->table[pos];
    s->table[pos] = sel;
    if (s->sorted)
        sorted_insert(st);
}

State set_popmin(Set s)
{
    Binode bn, prev;
    State st;

    bn = prev = BROOT;
    while (bn->left || bn->right) {
        while (bn->left) {
            prev = bn; bn = bn->left;
        }
        while (bn->right) {
            prev = bn; bn = bn->right;
        }
    }
    st = bn->st; bn->st = NULL;
    (prev->left == bn) ? (prev->left = NULL) : (prev->right = NULL);
    free(bn); set_remove(s, st);
    return st;
}

void set_remove(Set s, State st)
{
    size_t pos = hash(state_cfg(st));
    Setel sel = s->table[pos], prev = NULL;

    while (sel && sel->st != st) {
        prev = sel; sel = sel->next;
    }
    if (sel) {
        if (!prev) s->table[pos] = sel->next;
        else prev->next = sel->next;
        free(sel); // free(sel->st)?
    }
}

State set_in(Set s, State st)
{
    size_t pos = hash(state_cfg(st));
    Setel sel = s->table[pos];

    while (sel && sel->st != st)
        sel = sel->next;
    return sel->st;
}

void set_replace_state(Set s, State m, State n)
{
    State st = set_in(s, m);
    state_copy(st, n);
    free(n);
}
