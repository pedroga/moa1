#pragma once

#include <stdbool.h>

typedef unsigned long long CFG;
typedef struct state *State;

#define cfg_final 0x1234CDE5B0F6A987

#define cfg_at(cfg, pos) (((cfg) >> 4 * (15 - pos)) & 0xF)

State state_new(CFG);
int state_cmp(State, State);
bool state_final(State);
void state_sucgen(State);
State state_sucnext(void);
void state_hcalc(State, int (*)(CFG));
long long state_val(State);
CFG state_cfg(State);
void state_copy(State, State);
