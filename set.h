#pragma once

#include "state.h"

typedef struct set *Set;

Set set_new(bool sort);
void set_insert(Set, State *);
State set_popmin(Set);
State set_in(Set, State);
void set_remove(Set, State);
void set_replace_state(Set, State, State);

