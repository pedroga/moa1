#include <stdlib.h>
#include "state.h"

struct state {
    CFG cfg;
    int g, h;
    State daddy;
};

#define find_zero(cfg, pos) while ((cfg) & (pos)) (pos) <<= 4

State state_new(CFG cfg)
{
    State st = malloc(sizeof(struct state));

    st->cfg = cfg; st->g = 0; st->daddy = NULL;
    return st;
}

int state_cmp(State st1, State st2)
{
    return st1->g - st2->g;
}

bool state_final(State st)
{
    return st->cfg == cfg_final;
}

static CFG leftgen(CFG cfg)
{
    const CFG leftmost = 0xF000F000F000F000;
    CFG pos = 0xF;

    find_zero(cfg, pos); if (pos & leftmost) return 0;
    pos <<= 4; pos = cfg & pos; cfg = cfg & ~pos; pos >>= 4;
    return cfg | pos;
}

static CFG rightgen(CFG cfg)
{
    const CFG rightmost = 0x000F000F000F000F;
    CFG pos = 0xF;

    find_zero(cfg, pos); if (pos & rightmost) return 0;
    pos >>= 4; pos = cfg & pos; cfg = cfg & ~pos; pos <<= 4;
    return cfg | pos;
}

static CFG topgen(CFG cfg)
{
    const CFG topmost = 0xFFFF000000000000;
    CFG pos = 0xF;

    find_zero(cfg, pos); if (pos & topmost) return 0;
    pos <<= 4*4; pos = cfg & pos; cfg = cfg & ~pos; pos >>= 4*4;
    return cfg | pos;
}

static CFG bottomgen(CFG cfg)
{
    const CFG bottommost = 0x000000000000FFFF;
    CFG pos = 0xF;

    find_zero(cfg, pos); if (pos & bottommost) return 0;
    pos >>= 4*4; pos = cfg & pos; cfg = cfg & ~pos; pos <<= 4*4;
    return cfg | pos;
}

static CFG sucs[4];
static char lastsuc, nextsuc;
static State laststate;

void state_sucgen(State st)
{
    CFG cfg = st->cfg, x;
    int i;

    lastsuc = nextsuc = 0; /* !!! */laststate = st;
    if (x = leftgen(cfg)) sucs[lastsuc++] = x;
    if (x = rightgen(cfg)) sucs[lastsuc++] = x;
    if (x = topgen(cfg)) sucs[lastsuc++] = x;
    if (x = bottomgen(cfg)) sucs[lastsuc++] = x;
}

State state_sucnext(void)
{
    State st;

    if (nextsuc < lastsuc) {
        st = malloc(sizeof(struct state));
        st->cfg = sucs[nextsuc++];
        st->g = laststate->g + 1; st->daddy = laststate;
    } else st = NULL;
    return st;
}

void state_hcalc(State st, int (*heuristic)(CFG))
{
    st->h = heuristic(st->cfg);
}

long long state_val(State st)
{
    return st->g;
}

CFG state_cfg(State st)
{
    return st->cfg;
}

void state_copy(State d, State s)
{
    d->cfg = s->cfg;
    d->g = s->g; d->h = s->h;
    d->daddy = s->daddy;
}
