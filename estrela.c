/* file: heuristic.h */
/* author: pedro garozi */

#ifndef HEURISTIC_H
#define HEURISTIC_H

#include <stdbool.h>

typedef unsigned long long CFG;

struct State {
    CFG cfg;
    unsigned g, h;
    struct State *dad;
    struct State *next_hash;
};

#define TABLESIZE 1900

struct State *Open[TABLESIZE];
struct State *Closed[TABLESIZE];

/* inserts an element in the set */
void insert(struct State **Set, struct State *element);

/* returns the element of the set with minimal f = g + h and removes it from the sorted list */
struct State *popmin();

/* removes element from the set */
void set_remove(struct State **Set, CFG element);

/* generates the successors of v, retrievable with sucnext */
void sucgen(struct State *v);

/* returns next successor from last sucgen call or NULL if there are no more successors */
struct State *sucnext(void);

/* if the element is in the set returns it, NULL otherwise */
struct State *already_in(struct State **Set, CFG element);

/* WARNING: TO IMPROVE!! */
unsigned long long hash_func(CFG x);
/* --------------------- */


#endif /* not HEURISTIC_H */
/* file: heuristic.c */
/* author: pedro garozi */

#include <stdio.h>
#include <stdlib.h>

/* sets the hex in pos corresponding to the zero in x */
#define find_zero(x, pos) while (x & pos) pos <<= 4


unsigned long long hash_func(CFG x)
{
    x /= 23;
    return x % TABLESIZE;
}

static void insert_open(struct State *element)
{   unsigned long long hashpos = hash_func(element->cfg);

    if (! Open[hashpos]) {
        Open[hashpos] = element;
        element->next_hash = NULL;
    } else {
        struct State *st = Open[hashpos];
        int ef = element->g + element->h;

        if (st->g + st->h > ef) {
            element->next_hash = st;
            Open[hashpos] = element;
        } else {
            while (st->next_hash && st->next_hash->g + st->next_hash->h < ef)
                st = st->next_hash;
            element->next_hash = st->next_hash;
            st->next_hash = element;
        }
    }
}

static void insert_closed(struct State *element)
{   unsigned long long hashpos = hash_func(element->cfg);

    if (! Closed[hashpos]) {
        Closed[hashpos] = element;
        element->next_hash = NULL;
    } else {
        element->next_hash = Closed[hashpos];
        Closed[hashpos] = element;
    }
}

void insert(struct State *Set[], struct State *element)
{

    if (Set == Open) insert_open(element);
    else insert_closed(element);
}

struct State *popmin()
{   struct State *st = NULL;
    struct State *stmin = NULL;
    int i;

    for (i = 0; i < TABLESIZE && !st; ++i)
        st = Open[i];
    if (st) {
        stmin = st;
    }
    
    for (; i < TABLESIZE; ++i) {
        if (st && st->g + st->h < stmin->g + stmin->h)
            stmin = st;
        st = Open[i];
    }
    return stmin;
}

/* removes element from the set */
void set_remove(struct State **Set, CFG element)
{   unsigned long long hashpos = hash_func(element);

    if (Set[hashpos]) {
        struct State *st = Set[hashpos];

        if (!st->next_hash) {
            if (st->cfg == element) {
                if (Set == Closed) free(st);
                Set[hashpos] = NULL;
            }
        }
        else if (Set[hashpos]->cfg == element) {
            Set[hashpos] = Set[hashpos]->next_hash;
        } else {
            while (st->next_hash && st->next_hash->cfg != element)
                st = st->next_hash;
            if (st->next_hash) {
                if (Set == Closed) {
                    struct State *staux = st->next_hash;
                    free(staux);
                }
                st->next_hash = st->next_hash->next_hash;
            }
        }
    }
}

/* exchange the nibble at the left of 0 with 0 */
/* e.g. 1203 -> 1023 */
static CFG leftgen(CFG x)
{   unsigned long long pos = 0xF;

    find_zero(x, pos);

    /* if its at the leftmost position */
    if (pos & 0xF000F000F000F000)
        return 0;

    /* moves the nibble to the right */
    pos <<= 4;
    pos = x & pos;
    x = x & ~pos;
    pos >>= 4;
    x = x | pos;

    return x;
}

/* exchange the nibble at the right of 0 with 0 */
/* e.g. 1203 -> 1230 */
static CFG rightgen(CFG x)
{   unsigned long long pos = 0xF;

    find_zero(x, pos);
    
    /* if its at the rightmost position */
    if (pos & 0x000F000F000F000F)
        return 0;

    /* moves the nibble to the left */
    pos >>= 4;
    pos = x & pos;
    x = x & ~pos;
    pos <<= 4;
    x = x | pos;

    return x;
}

/* exchange the nibble at the top of 0 with 0 */
/* e.g. 4578_1203 -> 4508_1273 */
static CFG topgen(CFG x)
{   unsigned long long pos = 0xF;

    find_zero(x, pos);

    /* if its at the topmost position */
    if (pos & 0xFFFF000000000000)
        return 0;

    /* moves the nibble down */
    pos <<= 4*4; /* nibble_size * line_lenght */
    pos = x & pos;
    x = x & ~pos;
    pos >>= 4*4; /* nibble_size * line_lenght */
    x = x | pos;

    return x;
}

/* exchange the nibble at the bottom of 0 with 0 */
/* e.g. 4508_1273 -> 4578_1203 */
static CFG bottomgen(CFG x)
{   unsigned long long pos = 0xF;

    find_zero(x, pos);

    /* if its at the bottommost position */
    if (pos & 0x000000000000FFFF)
        return 0;

    /* moves the nibble up */
    pos >>= 4*4; /* nibble_size * line_lenght */
    pos = x & pos;
    x = x & ~pos;
    pos <<= 4*4; /* nibble_size * line_lenght */
    x = x | pos;

    return x;
}

static CFG successors[4];
static int sucindex;
static int lastindex;

void sucgen(struct State *v)
{   CFG cfg;
    sucindex = 0;
    lastindex = 0;
    
    cfg = leftgen(v->cfg);
    if (cfg)
        successors[sucindex++] = cfg;
    
    cfg = rightgen(v->cfg);
    if (cfg)
        successors[sucindex++] = cfg;
    
    cfg = topgen(v->cfg);
    if (cfg)
        successors[sucindex++] = cfg;
    
    cfg = bottomgen(v->cfg);
    if (cfg)
        successors[sucindex++] = cfg;
}

struct State *sucnext(void)
{
    if (lastindex < sucindex) {
        struct State *st = malloc(sizeof(struct State));
        st->cfg = successors[lastindex++];
        return st;
    } else {
        return NULL;
    }
}

struct State *already_in(struct State **Set, CFG element)
{   unsigned long long hashpos = hash_func(element);
    struct State *st = Set[hashpos];
    
    while (st) {
        if (st->cfg == element)
            return st;
        st = st->next_hash;
    }
    return NULL;
}
/* file: A*.c */
/* author: pedro garozi */

#include <stdio.h>
#include <stdlib.h>

#define SOLUTION 0x1234CDE5B0F6A987

#define cfg_at(cfg, pos) (((cfg) >> 4 * (15 - pos)) & 0xF)

int h3(CFG cfg)
{
    int i, v, p, count = 0;

    for (i = 0; i < 16; ++i) {
        if ((v = cfg_at(cfg, i))) {
            p = 0;
            while (cfg_at(SOLUTION, p) != v)
                ++p;
            count += abs(p/4 - i/4) + abs(p%4 - i%4);
        }
    } return count;
}

int main()
{   int (*heuristic)(CFG) = h3;

    CFG cfg;
    char input;
    int i;

    for (i = 0; i < 15; ++i) {
        scanf("%d", &input);
        cfg |= input; cfg <<= 4;
    } scanf("%d", &input); cfg |= input;

    struct State *first = malloc(sizeof(struct State));
    first->cfg = cfg;
    first->g = 0;
    first->h = heuristic(cfg);
    first->dad = NULL;
    first->next_hash = NULL;

    insert(Open, first);

    struct State *v, *m, *ml;
    while ((v = popmin()) && v->cfg != SOLUTION) {
        set_remove(Open, v->cfg);
        insert(Closed, v);

        sucgen(v);
        while ((m = sucnext())) {
            m->dad = v;
            m->g = v->g + 1;
            if (!(ml = already_in(Closed, m->cfg))) {
                m->h = heuristic(m->cfg);
                insert(Open, m);
            }
            else
                free(m);
        }
    }
    printf("%d\n", v->g);
}
