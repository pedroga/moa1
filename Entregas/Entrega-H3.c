#include <stdio.h>
#include <stdlib.h>

typedef long long int CFG;

CFG cfg_final = 0x1234CDE5B0F6A987;

#define cfg_at(cfg, pos) (((cfg) >> 4 * (15 - (pos))) & 0xF)

int h3(CFG cfg)
{
    int i, v, p, count = 0;

    for (i = 0; i < 16; ++i) {
        if ((v = cfg_at(cfg, i))) {
            p = 0;
            while (cfg_at(cfg_final, p) != v)
                ++p;
            count += abs(p/4 - i/4) + abs(p%4 - i%4);
        }
    } return count;
}

int main()
{
    int input, output, i;
    CFG cfg = 0;

    for (i = 0; i < 15; ++i) {
        scanf("%d", &input);
        cfg |= input; cfg <<= 4;
    } scanf("%d", &input); cfg |= input;
    output = h3(cfg);
    printf("%d\n", output);
}
