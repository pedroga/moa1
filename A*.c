/* file: A*.c */
/* author: pedro garozi */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include "set.h"
#include "heuristics.h"

int (*heuristic)(CFG) = h1;

int main()
{
    State v, m, ml;
    CFG cfg;
    int i;
    char input;
    Set Open = set_new(true), Closed = set_new(false);

    for (i = 0; i < 15; ++i) {
        scanf("%d", &input);
        cfg |= input; cfg <<= 4;
    } scanf("%d", &input); cfg |= input;

    v = state_new(cfg);
    set_insert(Open, &v);
    while ((v = set_popmin(Open)) && !state_final(v)) {
        state_sucgen(v), set_insert(Closed, &v);
        while (m = state_sucnext()) {
            state_hcalc(m, heuristic);
            if ((ml = set_in(Open, m))) {
                if (state_cmp(m, ml) < 0)
                    set_replace_state(Open, ml, m);
                else free(m);
            } else if (!(ml = set_in(Closed, m))) {
                set_insert(Open, &m);
	    } else
		free(m);
        }
    }
    printf("%ll\n", state_val(v));
}
