#include "heuristics.h"

int h1(CFG cfg)
{
    int i, count = 0;

    for (i = 0; i < 16; ++i) {
        if (cfg_at(cfg, i) != cfg_at(cfg_final, i) && cfg_at(cfg, i) != 0)
            ++count;
    } return count;
}

static CFG unroll(CFG cfg)
{
    CFG aux = 0;

    aux |= cfg_at(cfg, 0); aux <<= 4;
    aux |= cfg_at(cfg, 1); aux <<= 4;
    aux |= cfg_at(cfg, 2); aux <<= 4;
    aux |= cfg_at(cfg, 3); aux <<= 4;
    aux |= cfg_at(cfg, 7); aux <<= 4;
    aux |= cfg_at(cfg, 11); aux <<= 4;
    aux |= cfg_at(cfg, 15); aux <<= 4;
    aux |= cfg_at(cfg, 14); aux <<= 4;
    aux |= cfg_at(cfg, 13); aux <<= 4;
    aux |= cfg_at(cfg, 12); aux <<= 4;
    aux |= cfg_at(cfg, 8); aux <<= 4;
    aux |= cfg_at(cfg, 4); aux <<= 4;
    aux |= cfg_at(cfg, 5); aux <<= 4;
    aux |= cfg_at(cfg, 6); aux <<= 4;
    aux |= cfg_at(cfg, 10); aux <<= 4;
    aux |= cfg_at(cfg, 9); aux <<= 4;
}

int h2(CFG cfg)
{
    int i, count = 0;
    CFG unrolled = unroll(cfg);

    for (i = 0; i < 15; ++i) {
        if (cfg_at(unrolled, i)
            && cfg_at(unrolled, i) != cfg_at(unrolled, i + 1) + 1)
            ++count;
    }
}
