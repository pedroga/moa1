#include <stdio.h>

typedef long long int CFG;

CFG cfg_final = 0x1234CDE5B0F6A987;

#define cfg_at(cfg, pos) (((cfg) >> 4 * (15 - (pos))) & 0xF)

int h1(CFG cfg)
{
    char c;
    int count = 0;

    for (c = 0; c < 16; ++c) {
        if (cfg_at(cfg, c) != cfg_at(cfg_final, c) && cfg_at(cfg, c) != 0)
            ++count;
    } return count;
}

int main()
{
    int input, output, i;
    CFG cfg = 0;

    for (i = 0; i < 15; ++i) {
        scanf("%d", &input);
        cfg |= input; cfg <<= 4;
    } scanf("%d", &input); cfg |= input;
    output = h1(cfg);
    printf("%d\n", output);
}
