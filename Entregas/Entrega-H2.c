#include <stdio.h>

typedef long long int CFG;

CFG cfg_final = 0x1234CDE5B0F6A987;

#define cfg_at(cfg, pos) (((cfg) >> (4 * (15 - (pos)))) & 0xF)

static CFG unroll(CFG cfg)
{
    CFG aux = 0;

    aux |= cfg_at(cfg, 0); aux <<= 4;
    aux |= cfg_at(cfg, 1); aux <<= 4;
    aux |= cfg_at(cfg, 2); aux <<= 4;
    aux |= cfg_at(cfg, 3); aux <<= 4;
    aux |= cfg_at(cfg, 7); aux <<= 4;
    aux |= cfg_at(cfg, 11); aux <<= 4;
    aux |= cfg_at(cfg, 15); aux <<= 4;
    aux |= cfg_at(cfg, 14); aux <<= 4;
    aux |= cfg_at(cfg, 13); aux <<= 4;
    aux |= cfg_at(cfg, 12); aux <<= 4;
    aux |= cfg_at(cfg, 8); aux <<= 4;
    aux |= cfg_at(cfg, 4); aux <<= 4;
    aux |= cfg_at(cfg, 5); aux <<= 4;
    aux |= cfg_at(cfg, 6); aux <<= 4;
    aux |= cfg_at(cfg, 10); aux <<= 4;
    aux |= cfg_at(cfg, 9);
    return aux;
}

int h2(CFG cfg)
{
    int i, count = 0;
    CFG unrolled = unroll(cfg);

    for (i = 0; i < 15; ++i) {
        if (cfg_at(unrolled, i)
            && cfg_at(unrolled, i) + 1!= cfg_at(unrolled, i + 1)) {
            ++count;
        }
    } return count;
}

int main()
{
    int input, output, i;
    CFG cfg = 0;

    for (i = 0; i < 15; ++i) {
        scanf("%d", &input);
        cfg |= input; cfg <<= 4;
    } scanf("%d", &input); cfg |= input;
    output = h2(cfg);
    printf("%d\n", output);
}
